# Design

client for visual recognition of medical reports using artifical intelligence

competes: haut.ai

interacts: with users, AI API

constitutes: a web application and a mobile application for ios

includes: database for pictures and reports, UI, UI state storage

target audience: patients, members of hospitals, students

mission: ??? make experts' knowledges accessible for those doctors wno can't have/afford it 

stakeholder: Digital Vision Solutions LLC, Norcivilan Labs

questions for stakeholder:
- what is the main purpose/mission of the product?
- what are the indicators of estimating the purpose?
-what problems will the product solve for users?
- how must a final product look like?
- what is the timing?
- what is a marketing strategy?
-why this time (now) is the most appropriate for the debelopment of this product?
-do we need 2 options - registration for doctors and patients?
-is it advisable for patients to communicate through chat with doctors?
-we need a history of a single patient to make possible the investigation of changes by doctors

patterns: MVC

## android screens
 - login screen
   - logo
   - email input
   - password input
   - forgotten password link
   - login button
   - signup button
 - signup screen
   - email input
   - password input
   - city input
   - country dropdown
   - name input
   - surname input
   - patronim input
   - occupation dropdown
   - promocode input
   - signup button
 - menu screen
   - profile button
   - review button
   - retinopathy button
   - OKT button
 - profile screen
   - name
   - about me
     - country
     - city
     - email
   - occupation
     - position
     - organization
   - exit button
 - review screen
   - stars
   - feedback form
     - star rating
     - textbox
     - submit button
   - reviews list
     - card
       - stars
       - name
       - review
       - date
 - add detection menu screen
   - open file
   - take picture
   - open demo picture
 - detections screen (portrait)
   - exit button
   - photos gallery
 - detection screen (landscape)
   - (center) photo
     - view toggle (raw/processed)
     - left arrow button
     - right arrow button
   - (right) recognitions, list
     - recognition card
       - circle with color
       - label
   - (left) conclusions, list
     - conclusion card
       - label
       - status label (found/notfound)
     - chart card
       - label
       - chart
     - button link to full report
## iOS screens
 - login screen
   - logo
   - email input
   - password input
   - forgotten password link
   - submit button
   - signup button
 - signup screen
   - email input
   - password input
   - city input
   - country dropdown
   - name input
   - surname input
   - patronim input
   - occupation dropdown
   - promocode input
   - submit button
 - category screen
   - (top left) logout button to login screen
   - (top right) profile button
   - retinopathy category navigation item
   - OKT category navigation item
 - retinopathy/OKT detail screen
   - add from gallery button
   - take picture button
   - add demo button
   - list of detections
   - back button to menu screen
 - photo screen
   - back button to detection screen
   - (left) photo
     - (botton center) view toggle (raw/processed)
     - (top center) left arrow button
     - (top center) right arrow button
     - (right) report button (show/hide)
   - (right) report sidebar
     - (top) tab panel
       - recognitions tab
       - conclusions tab
     - recognitions, list
       - recognition card
         - circle with color
         - label
     - conclusions, list
       - conclusion card
         - label
         - status label (found/notfound)
       - chart card
         - label
         - chart
       - button link to full report
 - profile screen
   - name
   - about me
     - country
     - city
     - email
   - occupation
     - position
     - organization
   - back button to menu
